package projectday;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Flipkart {

	public static void main(String[] args) throws InterruptedException {
		// Launch browser
				System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
				//object creation
				ChromeDriver driver = new ChromeDriver();
				driver.manage().window().maximize();
				driver.get("https://www.flipkart.com");
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				driver.getKeyboard().sendKeys(Keys.ESCAPE);
				
				WebElement electronic = driver.findElementByXPath("//span[text()='Electronics']");
				WebElement mi = driver.findElementByXPath("//a[text()='Mi']");
				
				Actions builder = new Actions(driver);
				builder.moveToElement(electronic).pause(2000).perform();
				builder.click(mi).perform();
				Thread.sleep(2000);
				String title = driver.getTitle();
				System.out.println(title);
				
				if (title.contains("Mi Mobile Phones")) {
					System.out.println("Title matched");
				} else {
					System.out.println("Title does not matched");
				}
				
				driver.findElementByXPath("//div[text()='Newest First']").click();
				
				Thread.sleep(2000);  
				
				
				List<WebElement> brandNames = driver.findElementsByXPath("//div[@class='_3wU53n']");
				for (WebElement eachBrand : brandNames) {
					System.out.println(eachBrand.getText());
				}
				List<WebElement> price = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");
				for (WebElement eachPrice : price) {
					
					System.out.println(eachPrice.getText().replaceAll("\\D", ""));
				}
				
				String text = driver.findElementByXPath("//div[@class='_3wU53n']").getText();
				System.out.println(text);
				driver.findElementByXPath("//div[@class='_3wU53n']").click();
				Set<String> windowHandles = driver.getWindowHandles();
				List<String> lst = new ArrayList<>();
				lst.addAll(windowHandles);
				driver.switchTo().window(lst.get(1));
								
				String title2 = driver.getTitle();
				Thread.sleep(2000);
				System.out.println(title2);
				if (title2.contains(text)) {
				System.out.println("Title of the product matches");
				}
				else {
				System.out.println("Title of the product does not match");
				}
				WebElement reviews = driver.findElementByXPath("//span[@class='_38sUEc']");
				System.out.println("Reviews and rating of this product is "+reviews.getText());
				
				
				
				
				

	}

}
