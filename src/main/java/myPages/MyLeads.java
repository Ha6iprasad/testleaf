package myPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeads extends ProjectMethods{
	public MyLeads()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText="Create Lead") WebElement eleCrleads;
	public CreateLead ClickCrleads()
	{
		click(eleCrleads);
		return new CreateLead();
	}

}
