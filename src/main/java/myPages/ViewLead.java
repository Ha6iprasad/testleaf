package myPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLead extends ProjectMethods {

	public ViewLead()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="viewLead_companyName_sp") WebElement eleLeadId;
	public ViewLead viewLead()
	{
		System.out.println(eleLeadId.getText().replaceAll("\\d", ""));
		return this;
	}
}
