package myPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CrmSfa extends ProjectMethods{
	
	public CrmSfa()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText="CRM/SFA") WebElement eleCrm;
	public MyHomePage ClickCrm()
	{
		click(eleCrm);
		return new MyHomePage();
	}

}
