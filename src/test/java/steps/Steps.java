package steps;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Steps {
	public ChromeDriver driver;
@Given("Open the browser")
public void openTheBrowser() {
    
    System.setProperty("Webdriver.chrome,driver","./drivers/cromedriver.exe");
    driver = new ChromeDriver();
        		}

@Given("maximise the browser")
public void maximiseTheBrowser() {
	driver.manage().window().maximize();
     
}

@Given("set timeout")
public void setTimeout() {
    
    
}

@Given("Launch the URL")
public void launchTheURL() {
	driver.get("http://leaftaps.com/opentaps/");
    }

@Given("Enter the username as //(.*)")
public void enterTheUsername() {
	driver.findElementById("username").sendKeys("DemoSalesManager");
	       
}

@Given("Enter the password")
public void enterThePassword() {
	driver.findElementById("password").sendKeys ("crmsfa");
    
}

@Given("click on the login button")
public void clickOnTheLoginButton() {
	driver.findElementByClassName("decorativeSubmit").click();    
}

@Given("click crmsfa")
public void clickCrmsfa() {
	driver.findElementByLinkText("CRM/SFA").click();
 }

@Given("click leads")
public void clickLeads() {
	driver.findElementByLinkText("Leads").click();   
    }

@Given("click create leads")
public void clickCreateLeads() {
	driver.findElementByLinkText("create Leads").click();
    }

@Given("Enter company name")
public void enterCompanyName() {
	driver.findElementById("createLeadForm_companyName").sendKeys("TestLeaf"); 
}

@Given("Enter the First name")
public void enterTheFirstName() {
	driver.findElementById("createLeadForm_firstName").sendKeys("Hari");
    }

@Given("Enter the last name")
public void enterTheLastName() {
	driver.findElementById("createLeadForm_lastName").sendKeys("Prasad");
    }

@When("click on the create lead button")
public void clickOnTheCreateLeadButton() {
	driver.findElementByName("submitButton").click();
    }

@Then("Verify New lead is created")
public void verifyNewLeadIsCreated() {
        
}


}
